#!/usr/bin/python3
from __future__ import print_function
from sys import argv, stderr
import random
import string
from client import *
from random import randint

SERVICE_NAME = "askoracle"
OK, CORRUPT, MUMBLE, DOWN, CHECKER_ERROR = 101, 102, 103, 104, 110


def close(code, public="", private=""):
    if public:
        print(public)
    if private:
        print(private, file=stderr)
    print('Exit with code %d' % code, file=stderr)
    exit(code)


def check(*args):
    addr = args[0]
    ping(addr)
    auth = str(random.random())
    register(addr, auth)
    token, user_id = login(addr, auth)
    users = get_users(addr, token)

    found = list(filter(lambda x: x['_id'] == user_id, users))

    if not found:
        close(MUMBLE, "Users error", "User not found in response: %s" % (users))

    close(OK)


def put(*args):
    addr = args[0]
    flag_id = args[1]
    flag = args[2]

    register(addr, flag_id)
    token, user_id = login(addr, flag_id)

    put_public_quest(addr, token, user_id)
    put_private_quest(addr, token, user_id, flag)

    put_answers(addr, token)

    close(OK)


def get(*args):
    addr = args[0]
    flag_id = args[1]
    flag = args[2]

    public_user = str(random.random())
    register(addr, public_user)
    public_token, public_user_id = login(addr, public_user)

    flag_token, flag_user_id = login(addr, flag_id)
    public_answers = get_answers(addr, public_token, flag_user_id)

    answers = get_answers(addr, flag_token, flag_user_id)
    found = list(filter(lambda x: x['question'] == flag, answers))

    if not found:
        close(CORRUPT)

    close(OK)


def info(*args):
    close(OK, "vulns: 4")


def not_found(*args):
    print("Unsupported command %s" % argv[1], file=stderr)
    return CHECKER_ERROR

COMMANDS = {'check': check, 'put': put, 'get': get, 'info': info}

if __name__ == '__main__':
    try:
        COMMANDS.get(argv[1], not_found)(*argv[2:])
    except Exception as e:
        close(CHECKER_ERROR, "Evil checker", "INTERNAL ERROR: %s" % e)
