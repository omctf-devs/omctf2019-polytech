from sys import stderr
import requests
import UserAgents
import json
import hashlib
import random

OK, CORRUPT, MUMBLE, DOWN, CHECKER_ERROR = 101, 102, 103, 104, 110

def getMD5(str):
    m = hashlib.md5()
    m.update(str.encode('utf-8'))
    return m.hexdigest()


def getCred(id):
    user = getMD5(str(id) + 'polytech19user')
    password = getMD5(str(id) + 'polytech19pass')
    return user, password


def close(code, public="", private=""):
    if public:
        print(public)
    if private:
        print(private, file=stderr)
    print('Exit with code %d' % code, file=stderr)
    exit(code)


def ping(addr):
    url = 'http://%s:3000' % (addr)
    headers = {'User-Agent': UserAgents.get()}

    try:
        r = requests.get(url, headers=headers)

        if r.status_code != 200:
            close(MUMBLE, "Frontend status code error", "Invalid status code: %s %d" % (url, r.status_code))

        response_text = r.content.decode("UTF-8")
        etalon_text = '''<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="viewport" content="initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <title>Ask oracle</title>
  </head>
  <body>
    <div id="app"></div>
    <!-- built files will be auto injected -->
  <script type="text/javascript" src="/app.js"></script></body>
</html>'''
        if response_text.strip() != etalon_text:
            close(MUMBLE, "Frontend response error", "Invalid frontend response: %s %s" % (url, response_text))

    except Exception as e:
        close(DOWN, "Frontend HTTP Error", "Frontend HTTP error: %s" % e)


def register(addr, id):
    url = 'http://%s:3001/api/v1/signup' % (addr)
    headers = {'User-Agent': UserAgents.get()}
    user, password = getCred(id)
    creds = {"username": user, "password": password}

    try:
        r = requests.post(url, headers=headers, json=creds)

        if r.status_code != 200:
            close(MUMBLE, "Signup error", "Invalid status code: %s %d" % (url, r.status_code))

        str = r.content.decode("UTF-8")
        resp = json.loads(str)
        if resp['success'] != True:
            close(MUMBLE, "Signup error", "Invalid signup response: %s %s" % (url, resp))

    except Exception as e:
        close(DOWN, "Signup HTTP Error", "Signup HTTP error: %s" % e)


def login(addr, id):
    url = 'http://%s:3001/api/v1/auth' % (addr)
    headers = {'User-Agent': UserAgents.get()}
    user, password = getCred(id)
    creds = {"username": user, "password": password}

    try:
        r = requests.post(url, headers=headers, json=creds)

        if r.status_code != 200:
            close(MUMBLE, "Login error", "Invalid status code: %s %d" % (url, r.status_code))

        str = r.content.decode("UTF-8")
        resp = json.loads(str)

        if resp['success'] != True or (not 'token' in resp.keys()) or (not 'user' in resp.keys()):
            close(MUMBLE, "Login error", "Invalid login response: %s %s" % (url, resp))

        return resp['token'], resp['user']['_id']

    except Exception as e:
        close(DOWN, "Login HTTP Error", "Login HTTP error: %s" % e)


def put_public_quest(addr, token, user_id):
    url = 'http://%s:3001/api/v1/question' % (addr)
    headers = {'User-Agent': UserAgents.get(), 'Authorization': 'Bearer ' + str(token)}
    data = {'title': 'where flag?', 'type': '1', 'question': 'kek', 'answer_user_id': user_id}

    try:
        r = requests.post(url, headers=headers, json=data)

        if r.status_code != 200:
            close(MUMBLE, "Public quest error", "Invalid status code: %s %d" % (url, r.status_code))

        content = r.content.decode("UTF-8")
        resp = json.loads(content)
        if resp['success'] != True:
            close(MUMBLE, "Public quest error", "Invalid Public quest response: %s %s" % (url, resp))

    except Exception as e:
        close(DOWN, "Public quest HTTP Error", "Public quest HTTP error: %s" % e)


def put_private_quest(addr, token, user_id, flag):
    url = 'http://%s:3001/api/v1/question' % (addr)
    headers = {'User-Agent': UserAgents.get(), 'Authorization': 'Bearer ' + str(token)}
    data = {'title': 'flag', 'type': '0', 'question': flag, 'answer_user_id': user_id}

    try:
        r = requests.post(url, headers=headers, json=data)

        if r.status_code != 200:
            close(MUMBLE, "Private quest error", "Invalid status code: %s %d" % (url, r.status_code))

        content = r.content.decode("UTF-8")
        resp = json.loads(content)
        if resp['success'] != True:
            close(MUMBLE, "Private quest error", "Invalid Private quest response: %s %s" % (url, resp))

    except Exception as e:
        close(DOWN, "Private quest HTTP Error", "Private quest HTTP error: %s" % e)


def put_answers(addr, token):
    questions = get_questions(addr, token)
    url = 'http://%s:3001/api/v1/answer' % (addr)
    headers = {'User-Agent': UserAgents.get(), 'Authorization': 'Bearer ' + str(token)}

    try:
        for quest in questions:
            data = {'answer': 'cheburek', 'quest_id': quest['_id']}
            r = requests.post(url, headers=headers, json=data)

            if r.status_code != 200:
                close(MUMBLE, "Put answer error", "Invalid status code: %s %d" % (url, r.status_code))

            content = r.content.decode("UTF-8")
            resp = json.loads(content)
            if resp['success'] != True:
                close(MUMBLE, "Put answer error", "Invalid Put answer response: %s %s" % (url, resp))

    except Exception as e:
        close(DOWN, "Put answer HTTP Error", "Put answer HTTP error: %s" % e)


def get_questions(addr, token):
    url = 'http://%s:3001/api/v1/questions' % (addr)
    headers = {'User-Agent': UserAgents.get(), 'Authorization': 'Bearer ' + str(token)}

    try:
        r = requests.get(url, headers=headers)

        if r.status_code != 200:
            close(MUMBLE, "Questions get error", "Invalid Questions status code: %s %d" % (url, r.status_code))

        content = r.content.decode("UTF-8")
        resp = json.loads(content)
        if len(resp) == 0:
            close(MUMBLE, "Questions get error", "Invalid Questions get response: %s %s" % (url, resp))

        return resp

    except Exception as e:
        close(DOWN, "Questions HTTP Error", "Questions HTTP error: %s" % e)


def get_answers(addr, token, user_id = False):
    if user_id:
        url = 'http://%s:3001/api/v1/answers?user_id=%s' % (addr, user_id)
    else:
        url = 'http://%s:3001/api/v1/answers' % (addr)

    headers = {'User-Agent': UserAgents.get(), 'Authorization': 'Bearer ' + str(token)}

    try:
        r = requests.get(url, headers=headers)

        if r.status_code != 200:
            close(MUMBLE, "Answers get error", "Invalid Answers status code: %s %d" % (url, r.status_code))

        content = r.content.decode("UTF-8")
        resp = json.loads(content)
        if len(resp) == 0:
            close(MUMBLE, "Answers get error", "Invalid Answers get response: %s %s" % (url, resp))

        return resp

    except Exception as e:
        close(DOWN, "HTTP Error", "HTTP error: %s" % e)


def get_users(addr, token):
    url = 'http://%s:3001/api/v1/users' % (addr)
    headers = {'User-Agent': UserAgents.get(), 'Authorization': 'Bearer ' + str(token)}

    try:
        r = requests.get(url, headers=headers)

        if r.status_code != 200:
            close(MUMBLE, "Users error", "Invalid status code: %s %d" % (url, r.status_code))
        content = r.content.decode("UTF-8")
        resp = json.loads(content)
        if len(resp) == 0:
            close(MUMBLE, "Users error", "Invalid Users response: %s %s" % (url, resp))

        return resp

    except Exception as e:
        close(DOWN, "Users HTTP Error", "Users HTTP error: %s" % e)
