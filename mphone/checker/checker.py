#!/usr/bin/env python3
import sys
from sys import stderr
import numpy as np
import time
import socket
import hashlib
import random
# put-get flag to service success
def service_up():
    exit(101)

# service is available (available tcp connect) but protocol wrong could not put/get flag
def service_corrupt():
    exit(102)

# waited time (for example: 5 sec) but service did not have time to reply
def service_mumble():
    exit(103)

# service is not available (maybe blocked port or service is down)
def service_down():
    exit(104)

def GetMD5(str):
    m = hashlib.md5()
    m.update(str.encode('utf-8'))
    return m.hexdigest()

def GetCred(id):
    login1 = GetMD5(str(id) + 'narwhall')
    login2 = GetMD5(str(id) + 'narwhahh')
    password1 = GetMD5(str(id) + 'narwhallpass')
    password2 = GetMD5(str(id) + 'narwhapass')
    return login2, login1, password2, password1

def print_usage_and_exit():
    print("""
Usage:
    """ + sys.argv[0] + """ (put) <127.0.0.1> <asdfg> <123e4567-e89b-12d3-a456-426655440000>
    OR
    """ + sys.argv[0] + """ check <127.0.0.1>

Example:
    """ + sys.argv[0] + """ put "127.0.0.1" "abcdifghr" "123e4567-e89b-12d3-a456-426655440000"
    OR
    """ + sys.argv[0] + """ check "127.0.0.1"

""")
    exit(0)
   
if len(sys.argv) < 3:
    print_usage_and_exit()

port = 3333
command = sys.argv[1]
host = sys.argv[2]
f_id = ''
flag = ''

if len(sys.argv) > 3:
    f_id = sys.argv[3]

if len(sys.argv) > 4:
    flag = sys.argv[4]

def register(login, password, s):
    global host, port
    try:
        s.recv(20000).decode('utf8')
        s.send(('1' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send(('1' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send((login + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send((password + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send('\n'.encode('utf-8'))

    except Exception as e:
        print("Error: in register")
        print(e, file=stderr)
        service_mumble()

def auth(login, password, s):
    try:

        data = s.recv(2000).decode('utf8')
        if "Chat menu" in data:
            s.send(('1'+'\n').encode('utf-8'))
            s.recv(2000).decode('utf8')
            s.send(('2' + '\n').encode('utf-8'))
            s.recv(2000).decode('utf8')
            s.send((login + '\n').encode('utf-8'))
            s.recv(2000).decode('utf8')
            s.send((password + '\n').encode('utf-8'))
            s.recv(2000).decode('utf8')
            s.send('\n'.encode('utf-8'))
        else:
            s.send(('2' + '\n').encode('utf-8'))
            s.recv(2000).decode('utf8')
            s.send((login + '\n').encode('utf-8'))
            s.recv(2000).decode('utf8')
            s.send((password + '\n').encode('utf-8'))
            s.recv(2000).decode('utf8')
            s.send('\n'.encode('utf-8'))
    except Exception as e:
        print("Error: in auth")
        print(e, file=stderr)
        service_mumble()

def message(s):
    global flag, f_id

    try:
        s.recv(20000).decode('utf8')
        time.sleep(2)
        s.send(('4' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send(('2' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send(('1' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        login2, login1, password2, password1 = GetCred(f_id)
        s.send((login2 + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send((flag + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send('\n'.encode('utf-8'))
        s.send(('2' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf8')
        s.send(('2' + '\n').encode('utf-8'))
        service_up()
    except Exception as e:
        print('Error: in message')
        print(e, file=stderr)
        service_mumble()

def get_flag():
    global host, port, f_id, flag

    try:
        s = socket.socket()
        s2 = socket.socket()
        s.connect((host, port))
        s2.connect((host, port))
        login2, login1, password2, password1 = GetCred(f_id)
        auth(login1, password1, s)
        auth(login2, password2, s2)

        s.recv(200000)
        s.send(('4'+'\n').encode('utf-8'))
        s.recv(200000)
        s.send(('2'+'\n').encode('utf-8'))
        s.recv(200000)
        s.send(('2'+'\n').encode('utf-8'))
        s.recv(200000)
        s.send(('2'+'\n').encode('utf-8'))
        time.sleep(2)
        data = s.recv(20000).decode('utf-8')
        s2.recv(20000)
        s2.send(('4' + '\n').encode('utf-8'))
        s2.recv(20000)
        s2.send(('2' + '\n').encode('utf-8'))
        s2.recv(20000)
        s2.send(('2' + '\n').encode('utf-8'))
        s2.recv(20000)
        s2.send(('1' + '\n').encode('utf-8'))
        time.sleep(2)
        data4 = s2.recv(20000).decode('utf-8')
        if flag in data and flag in data4:
            service_up()
    except Exception as e:
        print('Error: in history')
        print(e, file=stderr)
        service_mumble()


def put_flag():
    global host, port, f_id, flag
    try:

        s1=socket.socket()
        s2=socket.socket()
        s1.connect((host, port))
        login2, login1, password2, password1 = GetCred(f_id)
        register(login1, password1, s1)
        auth(login1, password1, s1)
        s2.connect((host, port))
        register(login2, password2, s2)
        auth(login2, password2, s2)
        message(s1)


    except Exception as e:
        print("Error: ")
        print(e, file=stderr)
        service_mumble()


def check():
    try:
        s = socket.socket()
        s.connect((host,port))
        data = s.recv(20000).decode('utf-8')
        check = '''
  │    1 - User menu                                                        │
  │    2 - Chat menu                                                        │
  │    3 - Exit                                                             │
  │                                                                         │
  │                                                                         │
  └─────────────────────────────────────────────────────────────────────────┘
'''
        s.send(('1' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send(('1' + '\n').encode('utf-8'))
        login = GetMD5(str(random.random()))
        password = GetMD5(str(random.random()))
        s.recv(20000).decode('utf-8')
        s.send((login + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send((password + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send('\n'.encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send(('2' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send((login + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send((password + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send('\n'.encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send(('4' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send(('2' + '\n').encode('utf-8'))
        s.recv(20000).decode('utf-8')
        s.send(('3'+'\n').encode('utf-8'))
        data2 = s.recv(2000000).decode('utf-8')
        if check in data and login in data2:
            service_up()
        else:
            print('Face is broken')
            service_mumble()
    except Exception as e:
        print(e, file=stderr)
        service_down()
    service_down()
def try_connect():
    print("try_connect")
    global host, port
    try:
        sock=socket.socket()
        sock.connect((host,port))
        sock.recv(20000)
        sock.close()
        service_up()
    except Exception as e:
        print(e, file=stderr)
        service_down()
    service_down()


if command=="put":
    put_flag()
elif command=="get":
    get_flag()
elif command=="check":
    check()



